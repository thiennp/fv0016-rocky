App.config(function ($routeProvider) {

    $routeProvider.when(BASE_URL + "/mcommerce/mobile_sales_success/index/value_id/:value_id", {
        controller: 'MCommerceSalesSuccessViewController',
        templateUrl: BASE_URL + "/mcommerce/mobile_sales_success/template",
        code: "mcommerce-sales-success"
    });

}).controller('MCommerceSalesSuccessViewController', function ($scope, $location, $routeParams, $timeout) {

    $scope.$watch("isOnline", function (isOnline) {
        $scope.has_connection = isOnline;
    });

    $timeout(function() {
        $location.path(BASE_URL);
    }, 4000);

    $scope.value_id = $routeParams.value_id;

});