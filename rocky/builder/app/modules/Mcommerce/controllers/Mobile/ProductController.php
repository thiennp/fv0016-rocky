<?php

class Mcommerce_Mobile_ProductController extends Mcommerce_Controller_Mobile_Default {

    public function findAction() {

        if($value_id = $this->getRequest()->getParam("value_id") AND $product_id = $this->getRequest()->getParam("product_id")) {

            $product = new Catalog_Model_Product();
            $product->find($product_id);

            $data = array();

            if($product->getData("type") != "menu") {

                $minPrice = $product->getMinPrice();

                $optionsGroups = array();

                $current_store = $this->getStore();

                $taxRate = $current_store->getTax($product->getTaxId())->getRate();

                foreach($product->getGroups() as $group){
                    $optionsGroup = array(
                        "id" => $group->getId(),
                        "title" => $group->getTitle(),
                        "required" => $group->isRequired() === '1',
                        "options" => array()
                    );
                    foreach($group->getOptions() as $option){

                        $priceInclTax = $option->getPrice() * (1 + $taxRate / 100);

                        $optionsGroup["options"][] = array(
                            "id" => $option->getId(),
                            "optionId" => $option->getOptionId(),
                            "name" => $option->getName(),
                            "price" => (double) $option->getPrice(),
                            "formattedPrice" => $option->getPrice() > 0 ? $option->getFormattedPrice() : null,
                            "priceInclTax" => (double) $priceInclTax,
                            "formattedPriceInclTax" => $priceInclTax > 0 ? $product->formatPrice($priceInclTax) : null,

                        );
                    }
                    $optionsGroups[] = $optionsGroup;
                }

                $priceInclTax = $product->getPrice() * (1 + $taxRate / 100);

                $data = array( 
                    "product" => array(
                        "id" => $product->getId(),
                        "name" => $product->getName(),
                        "conditions" => $product->getConditions(),
                        "description" => $product->getDescription(),
                        "shortDescription" => strip_tags($product->getDescription()),
                        "price" => (float) $product->getPrice(),
                        "formattedPrice" => $product->getPrice() > 0 ? $product->getFormattedPrice() : null,
                        "priceInclTax" => (double) $priceInclTax,
                        "formattedPriceInclTax" => $priceInclTax > 0 ? $product->formatPrice($priceInclTax) : null,
                        "minPrice" => (float) $minPrice,
                        "formattedMinPrice" => $minPrice > 0 ? $product->formatPrice($minPrice) : null,
                        "picture" => $product->getPictureUrl(),
                        "optionsGroups" => $optionsGroups
                    ),
                    "page_title" => $this->_("Catalog")
                );

            }

            $this->_sendHtml($data);
        }
    }

}