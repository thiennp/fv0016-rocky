<?php

$data = array(
    array(
        "code" => "support_name",
        "label" => "Name"
    ),
    array(
        "code" => "sales_name",
        "label" => "Name"
    )
);

foreach($data as $configData) {

    $config = new System_Model_Config();
    $config->find($configData["code"], "code")
        ->setData($configData)
        ->save();

}
