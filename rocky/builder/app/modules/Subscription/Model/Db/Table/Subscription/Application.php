<?php

class Subscription_Model_Db_Table_Subscription_Application extends Core_Model_Db_Table {

    protected $_name = "subscription_application";
    protected $_primary = "subscription_app_id";

    public function getSubscriptionsList() {
//        echo 'ok:'.$this->_name;die();

        $arrayCol = array('subscription_app_id','is_active','expire_at','created_at');

        $select = $this->select()
            ->from(array('sa'=>$this->_name), $arrayCol)
            ->joinInner(array('s' => 'subscription'),'s.subscription_id = sa.subscription_id', array("sub_name" => new Zend_Db_Expr("s.name")) )
            ->joinInner(array('a' => 'application'),'a.app_id = sa.app_id', array("app_name" => new Zend_Db_Expr("a.name")) )
            ->joinInner(array('si' => 'sales_invoice'),'si.app_id = a.app_id', array() )
            ->joinInner(array('ad' => 'admin'),'ad.admin_id = si.admin_id', array("admin_name" => new Zend_Db_Expr("CONCAT(ad.firstname,' ',ad.lastname)")) )
            ->order("sa.created_at")
            ->group(array('subscription_app_id'))
//            ->group(array('sa.is_active', 'sa.expire_at', 'sa.created_at', 'sub_name', 'app_name', 'admin_name'))
            ->setIntegrityCheck(false)
        ;
//        print_r($select->assemble());

        return $this->fetchAll($select);

    }



}