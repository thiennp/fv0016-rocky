
App.factory('Push', function($rootScope, $http, httpCache, Url, AUTH_EVENTS) {

    var factory = {};

    factory.value_id = null;
    factory.pushs = 0;

    factory.findAll = function() {

        if(!this.value_id) return;

        return $http({
            method: 'GET',
            url: Url.get("push/mobile_list/findall", {value_id: this.value_id, device_uid: Application.device_uid}),
            cache: false,
            responseType:'json'
        }).success(function() {
            httpCache.remove(Url.get("push/mobile/count", {device_uid: Application.device_uid}));

        });
    };

    factory.getPushs = function(device_uid) {

        $http({
            method: 'GET',
            url: Url.get("push/mobile/count", {device_uid: device_uid}),
            cache: true,
            responseType: 'json'
        }).success(function (data) {
            factory.pushs = data.count;
            $rootScope.$broadcast(AUTH_EVENTS.unreadPushs);
        });
    };

    return factory;
});
