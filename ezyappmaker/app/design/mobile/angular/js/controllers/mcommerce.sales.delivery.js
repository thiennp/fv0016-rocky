App.config(function ($routeProvider) {

    $routeProvider.when(BASE_URL + "/mcommerce/mobile_sales_delivery/index/value_id/:value_id", {
        controller: 'MCommerceSalesDeliveryViewController',
        templateUrl: BASE_URL + "/mcommerce/mobile_sales_delivery/template",
        code: "mcommerce-sales-delivery"
    });

}).controller('MCommerceSalesDeliveryViewController', function ($scope, $routeParams, $location, McommerceCart, McommerceSalesDelivery, Message, Url) {

    $scope.$watch("isOnline", function (isOnline) {
        $scope.has_connection = isOnline;
        if (isOnline) {
            $scope.loadContent();
        }
    });

    $scope.is_loading = true;

    McommerceCart.value_id = $routeParams.value_id;
    McommerceSalesDelivery.value_id = $routeParams.value_id;
    $scope.value_id = $routeParams.value_id;

    $scope.page_title = 'Delivery';

    $scope.loadContent = function () {

        McommerceCart.find().success(function (data) {

            $scope.cart = data.cart;

            McommerceSalesDelivery.findStores().success(function (data) {

                $scope.stores = data.stores;

                $scope.selectedStore = data.stores.reduce(function (selectedStore, store) {
                    if ($scope.cart.storeId === store.id) {
                        selectedStore = store;
                    }

                    return selectedStore;
                }, null);

            }).finally(function () {
                $scope.is_loading = false;
            });

        }).error(function () {
            $scope.is_loading = false;
        });
    };

    $scope.setForm = function (form) {
        $scope.form = form;
    };

    $scope.selectedStoreUpdated = function() {
        if ($scope.form.selectedStore) {
            $scope.cart.deliveryMethodId = $scope.form.selectedStore.$modelValue.deliveryMethods.reduce(function (selectedDeliveryMethodId, deliveryMethod) {
                if (deliveryMethod.id === $scope.cart.deliveryMethodId) {
                    return $scope.cart.deliveryMethodId;
                }
                return selectedDeliveryMethodId;
            }, null);
        }
    };

    $scope.updateDeliveryInfos = function () {

        var form = $scope.form;

        form.submitted = true;

        if (form.$valid) {

            if(!$scope.is_loading) {
                $scope.is_loading = true;

                var postParameters = {
                    'delivery_method_id': $scope.cart.deliveryMethodId,
                    'store_id': $scope.selectedStore.id
                };

                McommerceSalesDelivery.updateDeliveryInfos(postParameters).success(function (data) {
                    $scope.goToPaymentPage();
                }).error(function (data) {
                    if (data && angular.isDefined(data.message)) {
                        $scope.message = new Message();
                        $scope.message.isError(true)
                            .setText(data.message)
                            .show();

                        $scope.is_loading = false;
                    }
                });
            }
        }
    };

    $scope.goToPaymentPage = function () {
        $location.path(Url.get("mcommerce/mobile_sales_payment/index", {
            value_id: $routeParams.value_id
        }));
    }

    $scope.header_right_button = {
        action: $scope.updateDeliveryInfos,
        title: "Next"
    };

    $scope.loadContent();

});