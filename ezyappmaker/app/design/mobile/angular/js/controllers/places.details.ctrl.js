App.config(function ($routeProvider) {

    $routeProvider.when(BASE_URL + "/places/mobile_details/index/value_id/:value_id/place_id/:place_id", {
        controller: 'PlacesDetailsController',
        templateUrl: BASE_URL + "/cms/mobile_page_view/template",
        code: "places-details cms"
    });

}).controller('PlacesDetailsController', function ($scope, $routeParams, $location, Cms, Message, ImageGallery, Url) {

    $scope.$watch("isOnline", function (isOnline) {
        $scope.has_connection = isOnline;
        $scope.loadContent();
    });

    $scope.gallery = ImageGallery;
    $scope.is_loading = true;

    $scope.value_id = Cms.value_id = $routeParams.value_id;

    $scope.loadContent = function () {
        Cms.findAllByPage($routeParams.place_id).success(function(data) {
            $scope.blocks = data.blocks;
            $scope.page_title = data.page_title;
        }).error(function() {

        }).finally(function() {
            $scope.is_loading = false;
        });
    };

    $scope.onShowMap = function (address) {
        $location.path(Url.get("places/mobile_detailsmap/index", {
            value_id: $routeParams.value_id,
            place_id: $routeParams.place_id
        }));
    };

    $scope.header_right_button = {
        action: $scope.goToMap,
        title: "Map"
    };

});