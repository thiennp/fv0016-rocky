<?php

class Application_Backoffice_ListController extends Backoffice_Controller_Default
{

    public function loadAction() {

        $html = array(
            "title" => $this->_("Applications"),
            "icon" => "fa-mobile",
        );

        $this->_sendHtml($html);

    }

    public function findallAction() {

        $application = new Application_Model_Application();
        $applications = $application->findAll();
        $app_ids = $application->findAllToPublish();
        $data = array();

        foreach($applications as $application) {
            $data[] = array(
                "id" => $application->getId(),
                "can_be_published" => in_array($application->getId(), $app_ids),
                "name" => mb_convert_encoding($application->getName(), 'UTF-8', 'UTF-8'),
                "bundle_id" => $application->getBundleId(),
                "icon" => $application->getIcon(128)
            );
        }

        $this->_sendHtml($data);

    }

    public function findbyadminAction() {

        $application = new Application_Model_Application();
        $applications = $application->findAllByAdmin($this->getRequest()->getParam("admin_id"));
        $data = array("app_ids" => array(), "is_allowed_to_add_pages" => array());

        foreach($applications as $application) {
            $data["app_ids"][] = $application->getId();
            if($application->getIsAllowedToAddPages()) {
                $data["is_allowed_to_add_pages"][] = $application->getId();
            }
        }

        $this->_sendHtml($data);

    }

}
