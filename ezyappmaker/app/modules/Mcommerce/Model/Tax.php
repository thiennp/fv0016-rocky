<?php

class Mcommerce_Model_Tax extends Core_Model_Default {

    public function __construct($datas = array()) {
        parent::__construct($datas);
        $this->_db_table = 'Mcommerce_Model_Db_Table_Tax';
    }

    public function findByStore($store_id) {
        return $this->getTable()->findByStore($store_id);
    }

    public function save() {

        parent::save();

        if($this->getStoreTaxes()) {

            $this->getTable()->beginTransaction();
            try {
                $this->getTable()->saveStoreTaxes($this->getId(), $this->getStoreTaxes());
                $this->getTable()->commit();
            }
            catch(Exception $e) {
                $this->getTable()->rollback();
                throw new Exception($this->_('An error occurred while saving. Please try again later.'));
            }

        }

        return $this;
    }

}