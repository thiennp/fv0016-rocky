<?php

$this->query("ALTER TABLE `comment` 
    ADD COLUMN `customer_id` int(11) UNSIGNED AFTER `value_id`,
    ADD COLUMN `latitude` DECIMAL(11,8) NULL DEFAULT NULL AFTER `is_visible`,
    ADD COLUMN `longitude` DECIMAL(11,8) NULL DEFAULT NULL AFTER `latitude`,
    ADD COLUMN `flag` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `longitude`;
");

$this->query("ALTER TABLE `comment_answer` 
    ADD COLUMN `flag` tinyint(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `text`;
");

$this->query("
    CREATE TABLE `comment_radius` (
        `radius_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
        `value_id` int(11) UNSIGNED NOT NULL,
        `radius` decimal(7,2) UNSIGNED DEFAULT 10,
        PRIMARY KEY (`radius_id`),
        KEY `KEY_VALUE_ID` (`value_id`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
");

$this->query("
    ALTER TABLE `comment_radius`
        ADD FOREIGN KEY `FK_VALUE_ID` (`value_id`) REFERENCES `application_option_value` (`value_id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

$library = new Media_Model_Library();
$library->setName('Fanwall')->save();

$icon_paths = array(
    '/fanwall/fanwall1.png',
    '/fanwall/fanwall2.png',
    '/fanwall/fanwall3.png',
    '/fanwall/fanwall4.png'
);

$icon_id = 0;
foreach($icon_paths as $key => $icon_path) {
    $datas = array('library_id' => $library->getId(), 'link' => $icon_path, 'can_be_colorized' => 1);
    $image = new Media_Model_Library_Image();
    $image->setData($datas)->save();

    if($key == 0) $icon_id = $image->getId();
}

$datas = array(
    'library_id' => $library->getId(),
    'icon_id' => $icon_id,
    'code' => 'fanwall',
    'name' => 'Fan Wall',
    'model' => 'Comment_Model_Comment',
    'desktop_uri' => 'comment/application/',
    'mobile_uri' => 'comment/mobile_list/',
    'only_once' => 0,
    'is_ajax' => 1,
    'position' => 10
);
$option = new Application_Model_Option();
$option->setData($datas)->save();
